package com.mojipay.utils

import com.mojipay.entities.Operation
import io.paperdb.Paper

class UserSession {

    companion object {

        fun logout() {
            Paper.book().delete("currentUser")
        }

        fun clearOperations() {
            Paper.book().delete("operations")
        }

        fun addOperation(operation: Operation) {
            val operations = getOperations()
            operations.add(operation)
            Paper.book().write("operations", operations)
        }

        fun removeOperation(operation: Operation) {
            var operations = getOperations()
            operations.remove(operation)
            Paper.book().write("operations", operations)
        }

        fun hasOperations(): Boolean = Paper.book().contains("operations")

        fun getOperations(): ArrayList<Operation> = Paper.book().read("operations", ArrayList())
        /*    fun isLoggedIn(): Boolean = Paper.book().contains("currentUser")

            fun getUser(): User? = Paper.book().read("currentUser", null)

            fun setUser(user: User?) {
                Crashlytics.setUserIdentifier(user?.id.toString())
    //            Crashlytics.setUserEmail(user?.email)
                Crashlytics.setUserName(user?.fname)
                Paper.book().write("currentUser", user)

            }*/
    }
}