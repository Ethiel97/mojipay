package com.mojipay.utils

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter


class ViewPagerAdapter(fm: FragmentManager?, titles: List<String> = ArrayList()) : FragmentPagerAdapter(fm) {
    private var fragmentList: MutableList<Fragment> = ArrayList()
    private var titles = titles

    override fun getItem(position: Int): Fragment = fragmentList[position]

    override fun getCount(): Int = fragmentList.size

    fun addFragment(fragment: Fragment) {
        fragmentList.add(fragment)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        if (titles.isNotEmpty()) {
            return this.titles[position]
        }
        return null
//        return super.getPageTitle(position)
    }
}