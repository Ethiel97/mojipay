package com.mojipay.utils;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static String dateToElapsed(String time) {

        DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime created_at = df.parseDateTime(time);

//        DateTime created_at = new DateTime(millis);
        Period period = new Period(created_at, DateTime.now());

        PeriodFormatterBuilder periodFormatter = new PeriodFormatterBuilder();
        if (period.getYears() != 0) {
            periodFormatter.appendYears().appendSuffix(" ans ");
        } else if (period.getMonths() != 0) {
            if (period.getMonths() <= 1)
                periodFormatter.appendMonths().appendSuffix(" mois ");
            else
                periodFormatter.appendMonths().appendSuffix(" mois ");
        } else if (period.getWeeks() != 0) {
            if (period.getWeeks() <= 1)
                periodFormatter.appendWeeks().appendSuffix(" sem ");
            else
                periodFormatter.appendWeeks().appendSuffix(" sems ");
        } else if (period.getDays() != 0) {
            if (period.getDays() <= 1)
                periodFormatter.appendDays().appendSuffix(" jr");
            else
                periodFormatter.appendDays().appendSuffix(" jrs ");
        } else if (period.getHours() != 0) {
            if (period.getHours() <= 1)
                periodFormatter.appendHours().appendSuffix(" h");
            else
                periodFormatter.appendHours().appendSuffix(" h ");
        } else if (period.getMinutes() != 0) {
            if (period.getMinutes() <= 1)
                periodFormatter.appendMinutes().appendSuffix(" min ");
            else
                periodFormatter.appendMinutes().appendSuffix(" mins ");
        } else if (period.getSeconds() != 0) {
            if (period.getSeconds() <= 1)
                periodFormatter.appendSeconds().appendSuffix(" sec ");
            else
                periodFormatter.appendSeconds().appendSuffix(" secs ");
        }
        PeriodFormatter formatter = periodFormatter.printZeroNever().toFormatter();

        return formatter.print(period).trim();
    }

    public static String getFormattedDate(Long dateTime) {
        SimpleDateFormat newFormat = new SimpleDateFormat("EEE, MMM dd yyyy");
        return newFormat.format(new Date(dateTime));
    }
}
