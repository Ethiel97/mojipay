package com.mojipay.utils

import android.support.multidex.MultiDexApplication
import com.google.firebase.FirebaseApp
import com.mojipay.R
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import io.paperdb.Paper
import net.danlew.android.joda.JodaTimeAndroid
import okhttp3.Cache
import okhttp3.OkHttpClient
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import java.io.File


class MojiPay : MultiDexApplication() {


    companion object {

        lateinit var picassoWithCache: Picasso

    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        JodaTimeAndroid.init(this);
        Paper.init(this)

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Poppins-SemiBold.ttf")
//                .setDefaultFontPath("fonts/Lato-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())

        /*  smsReceiver = SmsReceiver(SmsReceiver.SERVER_NUMBER)
          registerReceiver(smsReceiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION))*/

        /*   TypefaceUtil.overrideFont(applicationContext, "SERIF", "fonts/Montserrat-Regular.ttf")*/

        /*  val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))
          val locationJob = dispatcher.newJobBuilder()
                  .setService(LocationService::class.java) // the JobService that will be called
                  .setTag("location") // uniquely identifies the job
                  .setLifetime(Lifetime.FOREVER)
                  .setConstraints(Constraint.ON_ANY_NETWORK)
                  .build()

          dispatcher.schedule(locationJob)*/

        val httpCacheDirectory = File(cacheDir, "picasso-cache")
        val cache = Cache(httpCacheDirectory, 15 * 1024 * 1024)
        val okHttpClientBuilder = OkHttpClient.Builder().cache(cache)

        picassoWithCache = Picasso.Builder(this).downloader(OkHttp3Downloader(okHttpClientBuilder.build())).build()


    }


}

