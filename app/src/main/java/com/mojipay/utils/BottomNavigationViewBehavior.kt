package com.mojipay.utils

import android.support.design.widget.CoordinatorLayout
import android.support.v4.view.ViewCompat
import android.support.v7.widget.CardView
import android.view.View
import android.view.animation.LinearInterpolator


class BottomNavigationViewBehavior : CoordinatorLayout.Behavior<CardView>() {

    private var height: Int = 0

    override fun onLayoutChild(parent: CoordinatorLayout?, child: CardView?, layoutDirection: Int): Boolean {
        height = child!!.height
        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout,
                                     child: CardView, directTargetChild: View, target: View,
                                     axes: Int, type: Int): Boolean {
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL
    }

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout, child: CardView,
                                target: View, dxConsumed: Int, dyConsumed: Int,
                                dxUnconsumed: Int, dyUnconsumed: Int,
                                @ViewCompat.NestedScrollType type: Int) {
        if (dyConsumed > 0) {
            slideDown(child)
        } else if (dyConsumed < 0) {
            slideUp(child)
        }
    }

    private fun slideUp(child: CardView) {
        child.clearAnimation()
        child.animate().setInterpolator(LinearInterpolator()).translationY(0f).setDuration(150).start()
    }

    private fun slideDown(child: CardView) {
        child.clearAnimation()
        child.animate().setInterpolator(LinearInterpolator()).translationY(height.toFloat()).setDuration(150).start()
    }
}