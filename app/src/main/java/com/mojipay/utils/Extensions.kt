package com.mojipay.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.net.Uri
import android.provider.MediaStore.Images
import android.support.annotation.LayoutRes
import android.util.Base64
import android.util.Base64.DEFAULT
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.IOException


fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun File.encodeB64(file: File): String {

    var encodedFile: String = ""
    try {
        val fileInputStreamReader = FileInputStream(file)
        val bytes = ByteArray(file.length().toInt())
        fileInputStreamReader.read(bytes)
        encodedFile = Base64.encodeToString(bytes, DEFAULT)
//        Base64.getEncoder().encode()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    return encodedFile
}

fun Bitmap.mergeBitmaps(overlay: Bitmap, bitmap: Bitmap): Bitmap {
    val height = bitmap.height
    val width = bitmap.width
    val combined = Bitmap.createBitmap(width, height, bitmap.config)
    val canvas = Canvas(combined)
    val canvasWidth = canvas.width
    val canvasHeight = canvas.height
    canvas.drawBitmap(bitmap, Matrix(), null)
    val centreX = (canvasWidth - overlay.width) / 2.toFloat()
    val centreY = (canvasHeight - overlay.height) / 2.toFloat()
    canvas.drawBitmap(overlay, centreX, centreY, null)
    return combined
}


fun Bitmap.getImageUri(context: Context, inImage: Bitmap): Uri? {
    val bytes = ByteArrayOutputStream()
    inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val path = Images.Media.insertImage(context.contentResolver, inImage, "Title", null)
    return Uri.parse(path)
}