package com.mojipay.utils

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View


class FAB_Hide_On_Scroll(context: Context, attrs: AttributeSet) : FloatingActionButton.Behavior() {

    override fun layoutDependsOn(parent: CoordinatorLayout?, child: FloatingActionButton?, dependency: View?): Boolean {
        return dependency is RecyclerView

    }

    override fun onNestedScroll(coordinatorLayout: CoordinatorLayout, child: FloatingActionButton, target: View, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed)

        //child -> Floating Action Button
        if (dyConsumed > 0 && child.visibility == View.VISIBLE) {
            child.hide()
            /*   val layoutParams = child.layoutParams as CoordinatorLayout.LayoutParams
               val fab_bottomMargin = layoutParams.marginEnd

               child.animate().scaleX(0f).scaleX(0f).setDuration(100)

                       .setInterpolator(LinearInterpolator()).start()*/
            /* child.animate().translationY((child.height + fab_bottomMargin).toFloat()).setInterpolator(LinearInterpolator()).start()*/
        } else if (dyConsumed < 0 && child.visibility == View.GONE) {
            /* child.animate().scaleX(1f).scaleX(1f).setDuration(100).setInterpolator(LinearInterpolator()).start()*/
            child.show()

//            child.animate().translationY(0f).setInterpolator(LinearInterpolator()).start()
        }
    }

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout, child: FloatingActionButton, directTargetChild: View, target: View, nestedScrollAxes: Int): Boolean {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL
    }
}
