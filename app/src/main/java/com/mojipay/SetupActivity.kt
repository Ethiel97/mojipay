package com.mojipay

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.mojipay.adapters.MethodSpinnerAdapter
import com.mojipay.adapters.RecipientSpinnerAdapter
import com.mojipay.entities.Recipient
import com.mojipay.entities.TransferMethod
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_setup.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class SetupActivity : AppCompatActivity() {

    private lateinit var recipientSpinnerAdapter: RecipientSpinnerAdapter
    private lateinit var methodSpinnerAdapter: MethodSpinnerAdapter

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)

        this.initContent()
    }

    private fun initContent() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material)
        upArrow?.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)

        val methods: List<TransferMethod> = Paper.book().read("methods")
        val recipients: List<Recipient> = Paper.book().read("recipients")

        methodSpinnerAdapter = MethodSpinnerAdapter(this, R.layout.method_item_vertical, methods, resources)
        recipientSpinnerAdapter = RecipientSpinnerAdapter(this, R.layout.recipient_item_vertical, recipients, resources)

        method_spinner.adapter = methodSpinnerAdapter
        recipient_spinner.adapter = recipientSpinnerAdapter

        process.setOnClickListener {


            val method = method_spinner.selectedItem as TransferMethod
            val recipient = recipient_spinner.selectedItem as Recipient

            val bundle = Bundle()

//            val bids = getBids(project)
            bundle.putParcelable("RECEIVER", recipient)
            bundle.putParcelable("METHOD", method)
//            bundle.putParcelableArrayList("BIDS", bids)

            val intent = Intent(this, SendMoneyActivity::class.java)
            intent.putExtras(bundle)

            startActivity(intent)

        }

    }
}
