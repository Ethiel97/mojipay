package com.mojipay

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.CoordinatorLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.crashlytics.android.Crashlytics
import com.mojipay.fragments.AccountFragment
import com.mojipay.fragments.ActivityFragment
import com.mojipay.fragments.HomeFragment
import com.mojipay.fragments.RecipientsFragment
import com.mojipay.utils.BottomNavigationViewBehavior
import com.mojipay.utils.CustomViewPager
import com.mojipay.utils.ViewAnimation
import com.mojipay.utils.ViewPagerAdapter
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class MainActivity : AppCompatActivity() {
    private var rotate = false


    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)

        initContent()
    }


    private fun initContent() {
//        changeStatusBarColor()
//        setSupportActionBar(toolbar)
        val layoutParams = bottom_navigation_wrapper.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.behavior = BottomNavigationViewBehavior()

        disableShiftMode(bottom_navigation)

        /* supportFragmentManager.beginTransaction()
                 .replace(R.id.fragment_container, BrowseFragment.newInstance())
                 .addToBackStack(BrowseFragment.newInstance().toString())
                 .commit()
    */
        setupViewPager(viewPager)

        bottom_navigation.setOnNavigationItemSelectedListener {
            getSelectedBottomItemView(it)
            true
        }

        Handler().post {
            startIntroActivity()

        }
        /*  nav_view.setNavigationItemSelectedListener { item -> true }

          drawerLayout.setScrimColor(Color.TRANSPARENT)

          val toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open,
                  R.string.navigation_drawer_close)*/
        /*    val toggle = object : ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                private val scaleFactor = 6f

                override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                    super.onDrawerSlide(drawerView, slideOffset)
                    val slideX = drawerView.width * slideOffset
                    coordinator.translationX = slideX
                    coordinator.scaleX = slide_1 - slideOffset / scaleFactor
                    coordinator.scaleY = slide_1 - slideOffset / scaleFactor
                }
            }*/

        /*   drawerLayout.addDrawerListener(toggle)
           toggle.syncState()*/

        //init floating action buttons

        Handler().post {
            ViewAnimation.initShowOut(lyt_requestMoney)
            ViewAnimation.initShowOut(lyt_transferInter)
            ViewAnimation.initShowOut(lyt_transferCrossBorder)
        }

        back_drop.visibility = View.GONE

        fab_more.setOnClickListener { v -> toggleFabMode(v) }

        back_drop.setOnClickListener { toggleFabMode(fab_more) }

        fab_request.setOnClickListener {
            startActivity(Intent(this, SetupActivity::class.java))
        }

        fab_transferCrossBorder.setOnClickListener {
            startActivity(Intent(this, SetupActivity::class.java))
        }

        fab_transferIntern.setOnClickListener {
            startActivity(Intent(this, SetupActivity::class.java))
        }

        /*   fab_receive.setOnClickListener({
               startActivity(Intent(this, ReceiveMoneyActivity::class.java))
           })

           fab_send.setOnClickListener({
               startActivity(Intent(this, TransferMoneyActivity::class.java))
           })*/

    }

    private fun toggleFabMode(v: View) {
        rotate = ViewAnimation.rotateFab(v, !rotate)
        if (rotate) {
            ViewAnimation.showIn(lyt_transferCrossBorder)
            ViewAnimation.showIn(lyt_requestMoney)
            ViewAnimation.showIn(lyt_transferInter)
            back_drop.visibility = View.VISIBLE
        } else {
            ViewAnimation.showOut(lyt_transferCrossBorder)
            ViewAnimation.showOut(lyt_transferInter)
            ViewAnimation.showOut(lyt_requestMoney)
            back_drop.visibility = View.GONE
        }
    }

    @SuppressLint("RestrictedApi")
    private fun disableShiftMode(view: BottomNavigationView) {
        val menuView = view.getChildAt(0) as BottomNavigationMenuView
        try {
            val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
            shiftingMode.isAccessible = true
            shiftingMode.setBoolean(menuView, false)
            shiftingMode.isAccessible = false
            for (i in 0 until menuView.childCount) {
                val item = menuView.getChildAt(i) as BottomNavigationItemView
                item.setShiftingMode(false)
                // set once again checked value, so view will be updated
                item.setChecked(item.itemData.isChecked)
            }
        } catch (e: NoSuchFieldException) {
            Log.e("BNVHelper", "Unable to get shift mode field", e)
        } catch (e: IllegalAccessException) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e)
        }

    }

    private fun startIntroActivity() {
        val t = Thread {
            //  Initialize SharedPreferences
            val getPrefs = PreferenceManager
                    .getDefaultSharedPreferences(baseContext)
            //  Create a new boolean and preference and set it to true
            val isFirstStart = getPrefs.getBoolean("firstStart", true)

            //  If the activity has never started before...
            if (isFirstStart) {
                //  Launch app intro
                val i = Intent(this@MainActivity, AppIntroActivity::class.java)

                runOnUiThread { startActivity(i) }

                //  Make a new preferences editor
                /*   SharedPreferences.Editor e = getPrefs.edit();

        //  Edit preference to make it false because we don't want this to run again
//                e.putBoolean("firstStart", false);

        //  Apply changes
        e.apply();*/
            }
        }

        // Start the thread
        t.start()
    }

//    companion object {

    private fun getSelectedBottomItemView(item: MenuItem) {

        item.isChecked = true
        when (item.itemId) {
            R.id.nav_home -> {
                viewPager.currentItem = 0
            }

            R.id.nav_activity -> {
                viewPager.currentItem = 1
            }

            R.id.nav_account -> {
                viewPager.currentItem = 2
            }
        /* R.id.nav_activity -> {
             viewPager.currentItem = 0
         }
*/
            R.id.nav_recipients -> {
                viewPager.currentItem = 3
            }
        }

//        }
    }


    private fun setupViewPager(viewpager: CustomViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(HomeFragment.newInstance())
        adapter.addFragment(ActivityFragment.newInstance())
        adapter.addFragment(AccountFragment.newInstance())
//        adapter.addFragment(AccountFragment.newInstance())
        adapter.addFragment(RecipientsFragment.newInstance())
        viewpager.adapter = adapter
        viewpager.setPagingEnabled(false)
        viewPager.offscreenPageLimit = 0

        viewPager.currentItem = 3
        getSelectedBottomItemView(bottom_navigation.menu.getItem(0))

    }
}
