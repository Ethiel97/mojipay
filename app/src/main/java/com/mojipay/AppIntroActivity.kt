package com.mojipay

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import com.github.paolorotolo.appintro.AppIntro2
import com.mojipay.fragments.AppIntroSlider
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class AppIntroActivity : AppIntro2() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addSlide(AppIntroSlider.newInstance(R.layout.app_intro1));
        addSlide(AppIntroSlider.newInstance(R.layout.app_intro2));
        addSlide(AppIntroSlider.newInstance(R.layout.app_intro3));

        progressButtonEnabled
        showStatusBar(true);
        showSkipButton(true);

        showPagerIndicator(true)
        setBarColor(ContextCompat.getColor(this, android.R.color.transparent))

        // Turn vibration on and set intensity
        // You will need to add VIBRATE permission in Manifest file
        setVibrate(true);
        setVibrateIntensity(30);

        //Add animation to the intro slider
        setFadeAnimation()


        /*   askForPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), 1)*/

        /* Handler().postDelayed({

         }, 5000)*/
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        val getPrefs = PreferenceManager
                .getDefaultSharedPreferences(baseContext)
        //  Make a new preferences editor
        val e = getPrefs.edit()

        //  Edit preference to make it false because we don't want this to run again
        e.putBoolean("firstStart", false)
        e.apply()
//        startActivity(Intent(this, MainActivity::class.java))

        //        finishActivity(MainActivity.FIRST_START);
        finish()
    }

    override fun onNextPressed() {
        // Do something when users tap on Next button.
    }

    /* @Override
     public void onDonePressed() {
         // Do something when users tap on Done button.
         finishActivity(MainActivity.FIRST_START);
     }
 */

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        val getPrefs = PreferenceManager
                .getDefaultSharedPreferences(baseContext)
        //  Make a new preferences editor
        val e = getPrefs.edit()

        //  Edit preference to make it false because we don't want this to run again
        e.putBoolean("firstStart", false)
        e.apply()
//        startActivity(Intent(this, MainActivity::class.java))

        finish()
        //        finishActivity(MainActivity.FIRST_START);

    }

    override fun onSlideChanged() {
        // Do something when slide is changed
    }


}