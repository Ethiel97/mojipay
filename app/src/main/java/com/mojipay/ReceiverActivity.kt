package com.mojipay

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.mojipay.entities.Recipient
import com.mojipay.utils.MojiPay
import kotlinx.android.synthetic.main.activity_receiver.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

class ReceiverActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receiver)

        this.initContent()
    }

    private fun initContent() {

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material)
        upArrow?.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)

        val receiver: Recipient = intent.extras.getParcelable("RECEIVER")

        receiver_name.text = receiver.name?.first?.capitalize() + " " + receiver.name?.last?.capitalize()
        receiver_phone.text = receiver.phone
        receiver_address.text = receiver.location?.city?.capitalize() + " " + receiver.location?.state?.capitalize() + "\n\n" + receiver.location?.street?.capitalize()
        receiver_email.text = receiver.email
        MojiPay.picassoWithCache
                .load(receiver.picture?.large)
                .fit()
                ?.placeholder(R.drawable.gradient_background)
                ?.into(receiver_picture)

        sendMoney.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable("RECEIVER", receiver)
            val intent = Intent(this, SendMoneyActivity::class.java)
            intent.putExtras(bundle)

            startActivity(intent)
        }

        requestMoney.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable("SENDER", receiver)
            val intent = Intent(this, ReceiveMoneyActivity::class.java)
            intent.putExtras(bundle)

            startActivity(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
