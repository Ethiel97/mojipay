package com.mojipay

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Button
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog
import com.github.javiersantos.materialstyleddialogs.enums.Style
import com.mojipay.entities.Operation
import com.mojipay.entities.Recipient
import com.mojipay.utils.MojiPay
import com.mojipay.utils.UserSession
import kotlinx.android.synthetic.main.activity_receive_money.*
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.text.SimpleDateFormat
import java.util.*

class ReceiveMoneyActivity : AppCompatActivity() {

    private var firstTime = true

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receive_money)

        this.initContent()
    }

    private fun initContent() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material)
        upArrow?.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)

        val sender: Recipient = intent.extras.getParcelable("SENDER")

        val receiverName = sender.name?.first?.capitalize() + " " + sender.name?.last?.capitalize()
        receiver_name.text = receiverName
        MojiPay.picassoWithCache
                .load(sender.picture?.large)
                .fit()
                ?.placeholder(R.drawable.gradient_background)
                ?.into(receiver_pic)


        receiveMoney.setOnClickListener {

            val dialog = MaterialStyledDialog.Builder(this)
                    .setTitle("CONGRATS").setDescription("Félicitations $!\nVotre demande ${amount_textView.text.toString()} a bien été envoyée a $receiverName.")
                    .setStyle(Style.HEADER_WITH_TITLE).setHeaderDrawable(R.drawable.gradient_background)
                    .withDialogAnimation(true).setPositiveText("D'accord")
                    .onPositive { dialog, which ->
                        val calendar = Calendar.getInstance();
                        val mdformat = SimpleDateFormat("EEEE  dd/MM/yyyy HH:mm", Locale.ENGLISH)
                        val strDate = mdformat.format(calendar.time)

                        val operation = Operation("Request", strDate, amount_textView.text.toString(), sender)
                        UserSession.addOperation(operation)

                        dialog.dismiss()


                    }.show()

        }

    }

    fun setAmount(v: View) {
        val view = (v as Button)

        if (v.id != btn_clear.id) {
            if (firstTime) {
                amount_textView.text = ""
                firstTime = false
            }

            var number = view.text.toString().toInt()
            var text = amount_textView.text.toString() + number.toString()

            amount_textView.text = text
        } else
            amount_textView.text = ""


    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
