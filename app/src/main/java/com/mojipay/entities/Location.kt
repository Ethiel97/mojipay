package com.mojipay.entities

import android.os.Parcel
import android.os.Parcelable

data class Location(
	val city: String? = null,
	val street: String? = null,
	val timezone: Timezone? = null,
	val postcode: String? = null,
	val coordinates: Coordinates? = null,
	val state: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readParcelable(Timezone::class.java.classLoader),
			parcel.readString(),
			parcel.readParcelable(Coordinates::class.java.classLoader),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(city)
		parcel.writeString(street)
		parcel.writeParcelable(timezone, flags)
		parcel.writeString(postcode)
		parcel.writeParcelable(coordinates, flags)
		parcel.writeString(state)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Location> {
		override fun createFromParcel(parcel: Parcel): Location {
			return Location(parcel)
		}

		override fun newArray(size: Int): Array<Location?> {
			return arrayOfNulls(size)
		}
	}
}
