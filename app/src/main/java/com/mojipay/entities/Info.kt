package com.mojipay.entities

data class Info(
	val seed: String? = null,
	val page: Int? = null,
	val results: Int? = null,
	val version: String? = null
)
