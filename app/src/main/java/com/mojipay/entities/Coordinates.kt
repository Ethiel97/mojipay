package com.mojipay.entities

import android.os.Parcel
import android.os.Parcelable

data class Coordinates(
	val latitude: String? = null,
	val longitude: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(latitude)
		parcel.writeString(longitude)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Coordinates> {
		override fun createFromParcel(parcel: Parcel): Coordinates {
			return Coordinates(parcel)
		}

		override fun newArray(size: Int): Array<Coordinates?> {
			return arrayOfNulls(size)
		}
	}
}
