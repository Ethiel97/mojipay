package com.mojipay.entities

import android.os.Parcel
import android.os.Parcelable

data class Recipient(

		val phone: String? = null,
		val name: Name? = null,
		val email: String? = null,
		val picture: Picture? = null,
		val location: Location? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readParcelable(Name::class.java.classLoader),
			parcel.readString(),
			parcel.readParcelable(Picture::class.java.classLoader),
			parcel.readParcelable(Location::class.java.classLoader)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(phone)
		parcel.writeParcelable(name, flags)
		parcel.writeString(email)
		parcel.writeParcelable(picture, flags)
		parcel.writeParcelable(location, flags)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Recipient> {
		override fun createFromParcel(parcel: Parcel): Recipient {
			return Recipient(parcel)
		}

		override fun newArray(size: Int): Array<Recipient?> {
			return arrayOfNulls(size)
		}
	}
}