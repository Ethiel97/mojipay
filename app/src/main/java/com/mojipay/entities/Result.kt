package com.mojipay.entities

/**
 * Created by Ethiel on 15/03/2018.
 */
data class Result(val results: List<Recipient>,
                  val info: Info? = null
)
