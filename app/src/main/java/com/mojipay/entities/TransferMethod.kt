package com.mojipay.entities

import android.os.Parcel
import android.os.Parcelable

data class TransferMethod(var imageUrl: Int? = null, var name: String = "") : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(imageUrl)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TransferMethod> {
        override fun createFromParcel(parcel: Parcel): TransferMethod {
            return TransferMethod(parcel)
        }

        override fun newArray(size: Int): Array<TransferMethod?> {
            return arrayOfNulls(size)
        }
    }
}