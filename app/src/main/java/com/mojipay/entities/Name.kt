package com.mojipay.entities

import android.os.Parcel
import android.os.Parcelable

data class Name(
	val last: String? = null,
	val title: String? = null,
	val first: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(last)
		parcel.writeString(title)
		parcel.writeString(first)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Name> {
		override fun createFromParcel(parcel: Parcel): Name {
			return Name(parcel)
		}

		override fun newArray(size: Int): Array<Name?> {
			return arrayOfNulls(size)
		}
	}
}
