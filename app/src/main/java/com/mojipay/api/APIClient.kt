package com.mojipay.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object APIClient {
    //    public static final String BASE_URL = "http://192.168.137.1:8080/kassouaApp/public/api/";
//    val BASE_URL = "http://my.api.mockaroo.com/"
    val BASE_URL = "https://randomuser.me/"

    val API_KEY = "fd3ccd80"

    //public static final String BASE_URL = "http://172.30.91.134:8080/kassouaApp/public/api/";
    val PUT_METHOD = "PUT"
    private var retrofit: Retrofit? = null

    val retrofitClient: APIService?
        get() {
//            val httpCacheDirectory = File(context.getCacheDir(), "httpCache")
//            val cache = Cache(httpCacheDirectory, (10 * 1024 * 1024).toLong())
            val okHttpClient = OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(50, TimeUnit.SECONDS)
                    .readTimeout(50, TimeUnit.SECONDS)
                    .build()

            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
            }
            return retrofit!!.create(APIService::class.java)
        }
}