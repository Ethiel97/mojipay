package com.mojipay.api

import com.mojipay.entities.Result
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface APIService {

    @Headers("Accept: Application/json")
    @GET("api")
    fun getRecipients(
            @Query("inc")inc: String,
            @Query("results") results: String,
            @Query("nat") nat: String
    ): Call<Result>


}

