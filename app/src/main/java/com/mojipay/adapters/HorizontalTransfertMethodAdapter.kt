package com.mojipay.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.mojipay.R
import com.mojipay.SetupActivity
import com.mojipay.entities.TransferMethod
import com.mojipay.utils.inflate
import kotlinx.android.synthetic.main.transfer_method_horizontal_row_item.view.*

class HorizontalTransfertMethodAdapter(var methods: MutableList<TransferMethod> = mutableListOf()) : RecyclerView.Adapter<HorizontalTransfertMethodAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.transfer_method_horizontal_row_item, false)
        return ViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = methods.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindMethod(methods[position])

        holder.itemView.setOnClickListener {

            holder.itemView.context.startActivity(Intent(holder.itemView.context, SetupActivity::class.java))
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindMethod(method: TransferMethod) {

            itemView.method_name.text = method.name
            itemView.method_image.setImageResource(method.imageUrl!!)
        }

    }
}