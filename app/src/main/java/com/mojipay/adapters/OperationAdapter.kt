package com.mojipay.adapters

import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.github.florent37.expansionpanel.viewgroup.ExpansionLayoutCollection
import com.mojipay.R
import com.mojipay.entities.Operation
import com.mojipay.utils.inflate
import kotlinx.android.synthetic.main.operation_row_item.view.*

class OperationAdapter(var operations: MutableList<Operation> = mutableListOf()) : RecyclerView.Adapter<OperationAdapter.ViewHolder>() {

    private val expansionsCollection = ExpansionLayoutCollection()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.operation_row_item, false)
        return ViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = operations.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindOperation(operations[position])

        expansionsCollection.add(holder.itemView.expansionLayout)
        /* holder.itemView.bt_ok_panel.setOnClickListener({
           *//*  UserSession.removeOperation(operations[position])
            operations.removeAt(position)
            notifyDataSetChanged()

            if (itemCount == 0) {
                placeholder.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            }*//*


        })*/

    }

    companion object {
        fun putPlaceholder() {

        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindOperation(operation: Operation) {

            if (operation.name.equals("Request")) {

                itemView.amount.setTextColor(Color.RED)
                itemView.currency.setTextColor(Color.RED)
                itemView.operation_indicator.setImageResource(R.drawable.vector_drawable_reply)
                itemView.operation_indicator.background = ContextCompat.getDrawable(itemView.context, R.drawable.sky_circle)

            } else {
                itemView.amount.setTextColor(Color.GREEN)
                itemView.currency.setTextColor(Color.GREEN)
                itemView.operation_indicator.setImageResource(R.drawable.arrow_right)

                itemView.operation_indicator.background = ContextCompat.getDrawable(itemView.context, R.drawable.red_background)
            }

            itemView.mate_name.text = operation.mate?.name?.last?.capitalize() + " " + operation.mate?.name?.first
            itemView.amount.text = operation?.amount

            val generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
            val color1 = generator?.randomColor;
            val color2 = generator?.randomColor;

//            itemView.amount.setTextColor(generator.getColor())


// generate color based on a key (same key returns the same color), useful for list/grid views
//            int color2 = generator.getColor("user@gmail.com")

// declare the builder object once.
            val builder = TextDrawable.builder()
                    .beginConfig()
                    .width(50)
                    .height(50)
                    .endConfig()
                    .round()


// reuse the builder specs to create multiple drawables
            val drawable = builder.build(operation.mate?.name?.first?.substring(0, 1)?.toUpperCase(), color1!!);

            itemView.mate_pic.setImageDrawable(drawable)

            /*  MojiPay.picassoWithCache
                      .load(operation.mate?.picture?.large)
                      .fit()
                      ?.placeholder(R.drawable.gradient_background)
                      ?.into(itemView.mate_pic)*/

        }


    }
}