package com.mojipay.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mojipay.R;
import com.mojipay.entities.TransferMethod;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MethodSpinnerAdapter extends ArrayAdapter<String> {

    private Context context;
    private List data;
    TransferMethod method = null;
    public Resources res;
    LayoutInflater inflater;

    public MethodSpinnerAdapter(@NonNull Context context, int textViewResourceId, @NonNull List objects, Resources resLocal) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.data = objects;
        this.res = resLocal;
        inflater = (LayoutInflater.from(context));
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.method_item_vertical, parent, false);

        method = (TransferMethod) data.get(position);

        TextView methodName = row.findViewById(R.id.method_name);
        CircleImageView method_pic = row.findViewById(R.id.method_pic);

        methodName.setText(method.getName());
        try {
            method_pic.setImageResource(method.getImageUrl());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return row;

    }


    @Override
    public boolean isEnabled(int position) {
        return position != getCount();
    }
}
