package com.mojipay.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mojipay.R;
import com.mojipay.entities.Recipient;
import com.mojipay.utils.MojiPay;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecipientSpinnerAdapter extends ArrayAdapter<String> {

    private Context context;
    private List data;
    Recipient recipient = null;
    public Resources res;
    LayoutInflater inflater;

    public RecipientSpinnerAdapter(@NonNull Context context, int textViewResourceId, @NonNull List objects, Resources resLocal) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.data = objects;
        this.res = resLocal;
        inflater = (LayoutInflater.from(context));
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.recipient_item_vertical, parent, false);

        recipient = (Recipient) data.get(position);

        TextView recipientName = row.findViewById(R.id.recipient_name);
        CircleImageView recipientPic = row.findViewById(R.id.recipient_pic);


        recipientName.setText(recipient.getName().getFirst() + " " + recipient.getName().getLast());

        MojiPay.picassoWithCache.load(recipient.getPicture().getLarge())
                .placeholder(R.drawable.gradient_background)
                .into(recipientPic);

        return row;
    }

   /* @Override
    public int getCount() {
        //do not display the last item
        int count = super.getCount();
        return count;
//        return count > 0 ? count - 1 : count;
    }*/

    @Override
    public boolean isEnabled(int position) {
        return position != getCount();
    }
}
