package com.mojipay.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.mojipay.R
import com.mojipay.ReceiverActivity
import com.mojipay.entities.Recipient
import com.mojipay.utils.MojiPay
import com.mojipay.utils.inflate
import kotlinx.android.synthetic.main.recipient_row_item.view.*

class ReceiverAdapter(var context: Context, var recipients: List<Recipient> = ArrayList()) : RecyclerView.Adapter<ReceiverAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflatedView = parent.inflate(R.layout.recipient_row_item, false)
        return ViewHolder(inflatedView)

    }

    override fun getItemCount(): Int = recipients.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val recipient = recipients[position]
        holder.bindRecipient(recipient)


        holder.itemView.setOnClickListener {
            val bundle = Bundle()

//            val bids = getBids(project)
            bundle.putParcelable("RECEIVER", recipients[position])
//            bundle.putParcelableArrayList("BIDS", bids)

            val intent = Intent(holder.itemView.context, ReceiverActivity::class.java)
            intent.putExtras(bundle)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

                //                val pair1 = Pair.create<View, String>(holder.itemView.user_avatar, "user_avatar")
                //                val pair2 = Pair.create<View, String>(holder.itemView.project_title, "project_title")
                //                val pair3 = Pair.create<View, String>(holder.itemView.project_description, "project_description")
                //                val pair4 = Pair.create<View, String>(holder.itemView.username, "username")

                val pair5 = android.support.v4.util.Pair.create<View, String>(holder.itemView.receiver_pic, "receiver_pic")
                val pair6 = android.support.v4.util.Pair.create<View, String>(holder.itemView.recipient_name, "receiver_name")

                val compat = ActivityOptionsCompat.makeSceneTransitionAnimation(holder.itemView.context as Activity, pair5, pair6)
                holder.itemView.context.startActivity(intent, compat.toBundle())

            } else {
                holder.itemView.context.startActivity(intent)
            }
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindRecipient(recipient: Recipient) {

            itemView.recipient_name.text = "${recipient.name?.last} ${recipient.name?.first}"

            MojiPay.picassoWithCache
                    .load(recipient.picture?.large)
                    .fit()
                    ?.placeholder(R.drawable.gradient_background)
                    ?.into(itemView.receiver_pic)

        }
    }
}