package com.mojipay.fragments


import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mojipay.R
import com.mojipay.adapters.OperationAdapter
import com.mojipay.entities.Operation
import com.mojipay.utils.UserSession
import kotlinx.android.synthetic.main.fragment_activity.*


/**
 * A simple [Fragment] subclass.
 */
class ActivityFragment : Fragment() {

    private var operations: MutableList<Operation> = ArrayList()
    private lateinit var operationAdapter: OperationAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        operations = when {
            UserSession.hasOperations() -> UserSession.getOperations()
            else -> ArrayList()
        }
        operations.reverse()

        if (operations.isEmpty()) {
            operation_recyclerview.visibility = View.GONE
            empty_operations.visibility = View.VISIBLE
        } else {
            operation_recyclerview.visibility = View.VISIBLE
            empty_operations.visibility = View.GONE
        }

        operationAdapter = OperationAdapter(operations)

        operation_recyclerview.adapter = operationAdapter
        operationAdapter.notifyDataSetChanged()


//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        operation_recyclerview.layoutManager = LinearLayoutManager(context)
//        project_recyclerview.itemAnimator = SlideInUpAnimator()

        operation_recyclerview.setHasFixedSize(true)

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary)

        swipeRefresh.isRefreshing = false

        swipeRefresh.setOnRefreshListener {

            Handler().postDelayed({
                operations = UserSession.getOperations()
                operationAdapter = OperationAdapter(operations)
                operation_recyclerview.adapter = operationAdapter
                operationAdapter.notifyDataSetChanged()
                swipeRefresh.isRefreshing = false
            }, 2500)

        }
//        getData()
    }

    companion object {
        fun newInstance(): ActivityFragment = ActivityFragment()
    }

}// Required empty public constructor
