package com.mojipay.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mojipay.R
import com.mojipay.adapters.ReceiverAdapterVertical
import com.mojipay.api.APIClient
import com.mojipay.api.APIService
import com.mojipay.entities.Result
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_recipients.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 */
class RecipientsFragment : Fragment() {
    private lateinit var apiService: APIService

    private lateinit var adapter: ReceiverAdapterVertical


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recipients, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        apiService = APIClient.retrofitClient!!

        recipients_recyclerview.layoutManager = LinearLayoutManager(context)

        swipeRefresh.isRefreshing = true

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary)

        if (Paper.book().contains("recipients")) {
            adapter = ReceiverAdapterVertical(context!!, Paper.book().read("recipients"))

            adapter.notifyDataSetChanged()

            swipeRefresh.isRefreshing = false

            recipients_recyclerview.adapter = adapter
        } else {
            loadRecipients()
        }

        /* swipeRefresh.setOnRefreshListener {
             loadRecipients()
         }*/

    }

    companion object {
        fun newInstance(): RecipientsFragment = RecipientsFragment()
    }

    private fun loadRecipients() {
        apiService.getRecipients("name,email,picture,phone,location", "15", "us")
                .enqueue(object : Callback<Result> {
                    override fun onFailure(call: Call<Result>?, t: Throwable?) {
                        swipeRefresh.isRefreshing = false
//                        Toasty.error(context!!, t?.message!!, Toast.LENGTH_LONG).show()
                        android.util.Log.d("Erreur", t?.message)
                    }

                    override fun onResponse(call: Call<Result>?, response: Response<Result>?) {
                        swipeRefresh.isRefreshing = false
                        if (response?.isSuccessful!!) {

                            adapter = ReceiverAdapterVertical(context!!, response.body()?.results!!)

                            Paper.book().write("recipients", response.body()?.results)
                            adapter.notifyDataSetChanged()

                            recipients_recyclerview.adapter = adapter

                        }

                    }

                })
    }


}// Required empty public constructor
