package com.mojipay.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mojipay.MainActivity
import com.mojipay.R
import com.mojipay.adapters.HorizontalTransfertMethodAdapter
import com.mojipay.adapters.ReceiverAdapter
import com.mojipay.api.APIClient
import com.mojipay.api.APIService
import com.mojipay.entities.Result
import com.mojipay.entities.TransferMethod
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(), View.OnClickListener {
    private lateinit var apiService: APIService

    private lateinit var receiverAdapter: ReceiverAdapter
    private lateinit var methodAdapter: HorizontalTransfertMethodAdapter
    private var transferMethods: MutableList<TransferMethod> = ArrayList<TransferMethod>()

    private var names = listOf<String>("Mtn Money", "Visa", "Bitcoin")

    private var methods_images = listOf<String>("mtn_logo", "visa_1", "bitcoin")

    override fun onClick(v: View?) {

        when (v?.id) {
        /*   R.id.charge_balance -> {
               startActivity(Intent(context, LoginActivity::class.java))

           }
           R.id.request_money -> {
               startActivity(Intent(context, LoginActivity::class.java))

           }*/
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        apiService = APIClient.retrofitClient!!
        user_card.setBackgroundResource(R.drawable.card_background)

        method_recyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recipients_recyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

//        charge_balance.setOnClickListener(this)
        if (Paper.book().contains("recipients")) {
            receiverAdapter = ReceiverAdapter(context!!, Paper.book().read("recipients"))

            receiverAdapter.notifyDataSetChanged()

            recipients_recyclerview.adapter = receiverAdapter
        } else {
            loadRecipients()
        }

        getTransferMethodData()

        seeActivities.setOnClickListener {
            try {

                (context as MainActivity).viewPager.currentItem = 1
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
        fun getImageId(context: Context, imageName: String): Int = context.resources.getIdentifier("drawable/" + imageName, null, context.packageName)
    }

    private fun loadRecipients() {
        apiService.getRecipients("name,email,picture,phone,location", "15", "dk")
                .enqueue(object : Callback<Result> {
                    override fun onFailure(call: Call<Result>?, t: Throwable?) {
//                        Toasty.error(context!!, t?.message!!, Toast.LENGTH_LONG).show()
                        android.util.Log.d("Erreur", t?.message)
                    }

                    override fun onResponse(call: Call<Result>?, response: Response<Result>?) {
                        if (response?.isSuccessful!!) {

                            receiverAdapter = ReceiverAdapter(context!!, response.body()?.results!!)

                            Paper.book().write("recipients", response.body()?.results)
                            receiverAdapter.notifyDataSetChanged()

                            recipients_recyclerview.adapter = receiverAdapter

                        }

                    }

                })
    }

    private fun getTransferMethodData() {
        for (item in 0..2) {

            val transferMethod = TransferMethod()
            transferMethod.name = names[item]

            transferMethod.imageUrl = HomeFragment.getImageId(this.context!!, methods_images[item])
//            car.imageUrl =
            transferMethods.add(transferMethod)

        }

        Paper.book().write("methods", transferMethods)

        transferMethods.shuffle()

        methodAdapter = HorizontalTransfertMethodAdapter(transferMethods)
        methodAdapter?.notifyDataSetChanged()

        method_recyclerview.adapter = methodAdapter
    }

}// Required empty public constructor
